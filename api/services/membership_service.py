from typing import List

from sqlalchemy.ext.asyncio import AsyncSession

from api.schemas.membership_schemas import *
from api.models.membership_model import Membership
from api.commons.crud_service import CrudService

_OWNER_NOT_FOUND = f"Owner not found or deleted"

__all__ = ["MembershipService"]


class MembershipService(CrudService):
    def __init__(self, db_session: AsyncSession):
        super().__init__(db_session)
        self._model = Membership

    async def get_memberships(self) -> List[GetMembershipSchema]:
        try:
            return await super().gets(self._model)
        except Exception as e:
            raise MembershipServiceException(e, self.get_membership.__name__)

    async def get_membership(self, membership_id: str) -> GetMembershipSchema:
        try:
            return await super().get(self._model, membership_id)
        except Exception as e:
            raise MembershipServiceException(e, self.get_membership.__name__)

    async def add_membership(self, membership: PostPutMembershipSchema) -> GetMembershipSchema:
        try:
            return await super().post(self._model, membership)
        except Exception as e:
            raise MembershipServiceException(e, self.add_membership.__name__)

    async def update_membership(self, membership_id: str, membership: PostPutMembershipSchema) -> GetMembershipSchema:
        try:
            return await super().put(self._model, membership_id, membership)
        except Exception as e:
            raise MembershipServiceException(e, self.update_membership.__name__)

    async def delete_membership(self, membership_id: str) -> DeleteMembershipSchema:
        try:
            return await super().delete(self._model, membership_id, DeleteMembershipSchema)
        except Exception as e:
            raise MembershipServiceException(e, self.delete_membership.__name__)


class MembershipServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(message)
        print(f"Error on {function}\nError: {message}")