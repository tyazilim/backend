from typing import List

from sqlalchemy.ext.asyncio import AsyncSession

from api.schemas.owner_schemas import *
from api.models.owner_model import Owner
from api.commons.crud_service import CrudService

_OWNER_NOT_FOUND = f"Owner not found or deleted"

__all__ = ["OwnerService"]


class OwnerService(CrudService):
    def __init__(self, db_session: AsyncSession):
        super().__init__(db_session)
        self._model = Owner

    async def get_owners(self) -> List[GetOwnerSchema]:
        try:
            return await super().gets(self._model)
        except Exception as e:
            raise OwnerServiceException(e, self.get_owners.__name__)

    async def get_owner(self, owner_id: str) -> GetOwnerSchema:
        try:
            return await super().get(self._model, owner_id)
        except Exception as e:
            raise OwnerServiceException(e, self.get_owner.__name__)

    async def add_owner(self, owner: PostPutOwnerSchema) -> GetOwnerSchema:
        try:
            return await super().post(self._model, owner)
        except Exception as e:
            raise OwnerServiceException(e, self.add_owner.__name__)

    async def update_owner(self, owner_id: str, owner: PostPutOwnerSchema) -> GetOwnerSchema:
        try:
            return await super().put(self._model, owner_id, owner)
        except Exception as e:
            raise OwnerServiceException(e, self.update_owner.__name__)

    async def delete_owner(self, owner_id: str) -> DeleteOwnerSchema:
        try:
            return await super().delete(self._model, owner_id, DeleteOwnerSchema)
        except Exception as e:
            raise OwnerServiceException(e, self.delete_owner.__name__)


class OwnerServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(message)
        print(f"Error on {function}\nError: {message}")
