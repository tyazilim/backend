from typing import List

from sqlalchemy.ext.asyncio import AsyncSession

from api.schemas.customer_schemas import *
from api.models.customer_model import Customer
from api.commons import CrudService

_CUSTOMER_NOT_FOUND = f"Customer not found or deleted"

__all__ = ["CustomerService"]


class CustomerService(CrudService):
    def __init__(self, db_session: AsyncSession):
        super().__init__(db_session)
        self._model = Customer

    async def get_customers(self) -> List[GetCustomerSchema]:
        try:
            return await super().gets(self._model)
        except Exception as e:
            raise CustomerServiceException(e, self.get_customers.__name__)

    async def get_customer(self, customer_id: str) -> GetCustomerSchema:
        try:
            return await super().get(self._model, customer_id)
        except Exception as e:
            raise CustomerServiceException(e, self.get_customer.__name__)

    async def add_customer(self, customer: PostPutCustomerSchema) -> GetCustomerSchema:
        try:
            print("Add Customer")
            return await super().post(self._model, customer)
        except Exception as e:
            raise CustomerServiceException(e, self.add_customer.__name__)

    async def update_customer(self, customer_id: str, customer: PostPutCustomerSchema) -> GetCustomerSchema:
        try:
            return await super().put(self._model, customer_id, customer)
        except Exception as e:
            raise CustomerServiceException(e, self.update_customer.__name__)

    async def delete_customer(self, customer_id: str) -> DeleteCustomerSchema:
        try:
            return await super().delete(self._model, customer_id, DeleteCustomerSchema)
        except Exception as e:
            raise CustomerServiceException(e, self.delete_customer.__name__)


class CustomerServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(message)
        print(f"Error on {function}\nError: {message}")