from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api.db.session import sessionmanager
from api.routes import customer_router
from api.routes import membership_router
from api.routes import owner_router


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Function that handles startup and shutdown events.
    To understand more, read https://fastapi.tiangolo.com/advanced/events/
    """
    yield
    if sessionmanager.engine is not None:
        # Close the DB connection
        await sessionmanager.close()


app = FastAPI(lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_headers=["*"])


@app.get("/")
async def main():
    return "Hello World"


app.include_router(owner_router)
app.include_router(customer_router)
app.include_router(membership_router)

if __name__ == "__main__":
    uvicorn.run("api.main:app", host="0.0.0.0", reload=True, port=8000)
