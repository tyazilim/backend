from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from api.services.owner_service import OwnerService
from api.db.session import get_db_session
__all__ = ["inject_owner_service"]


def inject_owner_service(session: AsyncSession = Depends(get_db_session)):
    return OwnerService(session)

