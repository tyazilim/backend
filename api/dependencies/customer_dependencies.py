from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from api.services.customer_service import CustomerService
from api.db.session import get_db_session

__all__ = ["inject_customer_service"]


def inject_customer_service(session: AsyncSession = Depends(get_db_session)):
    return CustomerService(session)
