from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from api.services.membership_service import MembershipService
from api.db.session import get_db_session
__all__ = ["inject_membership_service"]


def inject_membership_service(session: AsyncSession = Depends(get_db_session)):
    return MembershipService(session)
