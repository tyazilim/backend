import uuid
from typing import TYPE_CHECKING


from sqlalchemy import Date, UUID, ForeignKey, Integer
from sqlalchemy.orm import Mapped, mapped_column, relationship

if TYPE_CHECKING:
    from api.models.customer_model import Customer
from api.db import Base

__all__ = [
    "Membership"
]


class Membership(Base):
    __tablename__ = "memberships"

    id: Mapped[UUID] = mapped_column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4()
    )
    customerId: Mapped[UUID] = mapped_column(ForeignKey("customers.id"))
    beginDate: Mapped[Date] = mapped_column(Date, nullable=False)
    duration: Mapped[Integer] = mapped_column(Integer, nullable=False)
    endDate: Mapped[Date] = mapped_column(Date, nullable=False)
    customer: Mapped["Customer"] = relationship(back_populates="memberships", lazy="selectin")

    __table__args = (
        {'extend_existing': True}
    )
