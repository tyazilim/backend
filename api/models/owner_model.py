import uuid
from sqlalchemy import UUID, String
from api.db.base import Base
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column

__all__ = ['Owner']


class Owner(Base):
    __tablename__ = 'owners'
    __table_args__ = {'extend_existing': True}

    id: Mapped[UUID] = mapped_column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username: Mapped[String] = mapped_column(
        String(255), nullable=False, unique=True)
    password: Mapped[String] = mapped_column(String(255), nullable=False)
