import datetime
import uuid
from typing import List
from typing import TYPE_CHECKING


from sqlalchemy import UUID, String, Date, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship
if TYPE_CHECKING:
    from api.models.membership_model import Membership

from api.db import Base

__all__ = ["Customer"]



class Customer(Base):
    __tablename__ = "customers"

    id: Mapped[UUID] = mapped_column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4()
    )
    name: Mapped[String] = mapped_column(String(255), nullable=False, unique=False)
    phone: Mapped[String] = mapped_column(String(255), nullable=False, unique=True)
    email: Mapped[String] = mapped_column(String(255), nullable=True, unique=True)
    registrationDate: Mapped[Date] = mapped_column(
        Date, nullable=False, unique=False, default=datetime.datetime
    )
    memberships: Mapped[List["Membership"]] = relationship(back_populates="customer", lazy="joined")

    __table__args = (
        UniqueConstraint("name", "phone", name="customer_uc"),
        {'extend_existing': True}
    )


