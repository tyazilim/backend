import uuid
from pydantic import BaseModel, field_validator, field_serializer, ConfigDict

__all__ = ['PostPutOwnerSchema', 'GetOwnerSchema', 'DeleteOwnerSchema']

config = ConfigDict(
    use_enum_values= True,
    from_attributes= True
)


class PostPutOwnerSchema(BaseModel):
    model_config = config
    username: str
    password: str


class GetOwnerSchema(BaseModel):
    model_config = config
    id: uuid.UUID
    username: str
    password: str

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)


class DeleteOwnerSchema(BaseModel):
    model_config = config
    id: uuid.UUID

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)
