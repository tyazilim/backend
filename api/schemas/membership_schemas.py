import datetime
import uuid

from pydantic import BaseModel, field_validator, field_serializer, ConfigDict

__all__ = ['PostPutMembershipSchema', 'GetMembershipSchema', 'DeleteMembershipSchema']

config = ConfigDict(
    use_enum_values=True,
    from_attributes=True
)


class PostPutMembershipSchema(BaseModel):
    model_config = config
    customerId: uuid.UUID
    beginDate: datetime.datetime
    duration: int
    endDate: datetime.datetime

    @field_serializer("customerId", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)


class GetMembershipSchema(BaseModel):
    model_config = config
    id: uuid.UUID
    customerId: uuid.UUID
    beginDate: datetime.datetime
    duration: int
    endDate: datetime.datetime
    customer: "GetCustomerSchema"

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)

    @field_serializer("customerId", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)


class DeleteMembershipSchema(BaseModel):
    model_config = config
    id: uuid.UUID

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)


from api.schemas.customer_schemas import GetCustomerSchema
GetMembershipSchema.update_forward_refs()




