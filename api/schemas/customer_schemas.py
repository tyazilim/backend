import datetime
import uuid
from typing import List

from pydantic import BaseModel, field_validator, field_serializer, ConfigDict

__all__ = ['PostPutCustomerSchema', 'GetCustomerSchema', 'DeleteCustomerSchema']

config = ConfigDict(
    use_enum_values=True,
    from_attributes=True
)


class PostPutCustomerSchema(BaseModel):
    model_config = config
    name: str
    phone: str
    email: str = None
    registrationDate: datetime.datetime


class GetCustomerSchema(BaseModel):
    model_config = config
    id: uuid.UUID
    phone: str
    email: str = None
    registrationDate: datetime.datetime
    memberships: "List[GetMembershipSchema]"

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)



class DeleteCustomerSchema(BaseModel):
    model_config = config
    id: uuid.UUID

    @field_serializer("id", when_used='json')
    def id_serializer(self, value: uuid):
        return str(value)


from api.schemas.membership_schemas import GetMembershipSchema
GetCustomerSchema.update_forward_refs()
