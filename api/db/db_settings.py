import os
from dotenv import load_dotenv
from pydantic_settings import BaseSettings
from pathlib import Path

__all__ = ["DBSettings", "settings"]

env_path = Path('./.env')
load_dotenv(dotenv_path=env_path)


class DBSettings(BaseSettings):
    DB_USERNAME: str = os.getenv("DB_USERNAME", "postgres")
    DB_PASSWORD: str = os.getenv("DB_PASSWORD", "postgres")
    DB_HOST: str = os.getenv("DB_HOST", "localhost")
    DB_PORT: int = os.getenv("DB_PORT", 5432)  # default postgres port is 5432
    DB_NAME: str = os.getenv("DB_NAME", "test")
    DB_DRIVER_NAME: str = os.getenv("DB_DRIVER_NAME", "postgresql")
    DATABASE_URL: str = f"{DB_DRIVER_NAME}://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"


settings = DBSettings()
