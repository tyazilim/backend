from typing import Any
from sqlalchemy.orm import as_declarative

from sqlalchemy import DateTime, func, Boolean
from sqlalchemy.orm import declarative_mixin
from sqlalchemy import MetaData
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column

__all__ = ["Base"]


@declarative_mixin
class TimestampMixin:
    created_at: Mapped[DateTime] = mapped_column(DateTime, nullable=False, default=func.now())
    updated_at: Mapped[DateTime] = mapped_column(DateTime, nullable=False, default=func.now())
    deleted: Mapped[Boolean] = mapped_column(Boolean, nullable=True, default=False)
    deleted_at: Mapped[DateTime] = mapped_column(DateTime, nullable=True, default=None)


@as_declarative()
class Base(TimestampMixin):
    metadata = MetaData()
    id: Mapped[Any]
    __name__: str

    def __repr__(self) -> str:
        representation: str = f"{self.__class__.__name__}("
        for k, v in self.__dict__.items():
            if k != "created_at" and k != "updated_at" and k != "deleted_at" and k != "deleted":
                representation += f"{k.upper()}: {v}, "

        representation += ")"
        return representation
