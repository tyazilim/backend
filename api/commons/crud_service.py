from typing import List, Any

from pydantic import BaseModel
from sqlalchemy import Executable, select, insert, literal_column, update, delete
from sqlalchemy.ext.asyncio import AsyncSession

from api.db import Base

_NOT_FOUND = f"Record not found"

__all__ = [
    "CrudService"
]


class CrudService:
    def __init__(self, db_session: AsyncSession):
        self._session = db_session

    @property
    def session(self):
        return self._session

    async def gets(self, model: Base) -> List[Any]:
        try:
            stmt: Executable = select(model).where(model.deleted is not False)
            records: List[BaseModel] = list((await self._session.scalars(stmt)).unique().all())
            return records
        except Exception as e:
            raise CrudServiceException(e, self.get.__name__)

    async def get(self, model: Base, record_id: str) -> Any:
        try:
            stmt: Executable = select(model).where(
                model.id == record_id, model.deleted is not False
            )
            record: BaseModel = (await self._session.scalars(stmt)).unique().first()
            if not record:
                raise CrudServiceException(
                    _NOT_FOUND, self.gets.__name__)
            return record
        except Exception as e:
            raise CrudServiceException(e, self.gets.__name__)

    async def post(self, model: Base, data: BaseModel) -> Any:
        try:
            stmt: Executable = insert(model).values(
                **data.model_dump()).returning(literal_column('*'))
            new_record: BaseModel = (await self._session.execute(stmt)).first()
            await self._session.commit()
            return new_record
        except Exception as e:
            raise CrudServiceException(e, self.post.__name__)

    async def put(self, model: Base, record_id: str, data: BaseModel) -> Any:
        try:
            stmt: Executable = (update(model)
                                .where(model.id == record_id)
                                .values(**data.model_dump()).returning(literal_column('*')))
            updated_record: BaseModel = (await self._session.execute(stmt)).first()
            await self._session.commit()
            return updated_record
        except Exception as e:
            raise CrudServiceException(e, self.put.__name__)

    async def delete(self, model: Base, record_id: str, return_model) -> Any:
        try:
            stmt: Executable = (delete(model)
                                .where(model.id == record_id)
                                .returning(model.id))
            deleted_record_id: str = (await self._session.scalars(stmt)).first()
            await self._session.commit()
            return return_model(id=deleted_record_id)
        except Exception as e:
            raise CrudServiceException(e, self.delete.__name__)


class CrudServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(message)
        print(f"Error on {function}\nError: {message}")
