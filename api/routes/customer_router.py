from typing import List

from fastapi import APIRouter, HTTPException, Depends

from api.services import CustomerService
from api.dependencies import inject_customer_service
from api.schemas.customer_schemas import *

__all__ = ["customer_router"]

customer_router = APIRouter(prefix="/customers", tags=["customers"], dependencies=[])


@customer_router.get("", response_model=List[GetCustomerSchema], include_in_schema=False)
@customer_router.get("/", response_model=List[GetCustomerSchema])
async def get_customers(service: CustomerService = Depends(inject_customer_service)):
    try:
        result: List[GetCustomerSchema] = await service.get_customers()
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@customer_router.get("/{customer_id}", response_model=GetCustomerSchema)
async def get_customer(customer_id: str, service: CustomerService = Depends(inject_customer_service)):
    try:
        result: GetCustomerSchema = await service.get_customer(customer_id)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@customer_router.post("", response_model=GetCustomerSchema, include_in_schema=False)
@customer_router.post("/", response_model=GetCustomerSchema)
async def create_customer(data: PostPutCustomerSchema, service: CustomerService = Depends(inject_customer_service)):
    try:
        return await service.add_customer(data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@customer_router.put("/{customer_id}", response_model=GetCustomerSchema)
async def update_customer(customer_id: str,
                          data: PostPutCustomerSchema,
                          service: CustomerService = Depends(inject_customer_service)):
    try:
        return await service.update_customer(customer_id, data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@customer_router.delete("/{customer_id}")
async def delete_customer(customer_id: str,
                          service: CustomerService = Depends(inject_customer_service)) -> DeleteCustomerSchema:
    try:
        return await service.delete_customer(customer_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
