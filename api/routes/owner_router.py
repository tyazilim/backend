from typing import List

from dependency_injector.wiring import inject, Provide
from fastapi import APIRouter, HTTPException, Depends

from api.services import OwnerService
from api.dependencies import inject_owner_service
from api.schemas.owner_schemas import *

__all__ = ["owner_router"]


owner_router = APIRouter(prefix="/owners", tags=["owners"], dependencies=[])


@owner_router.get("", response_model=List[GetOwnerSchema], include_in_schema=False)
@owner_router.get("/", response_model=List[GetOwnerSchema])
@inject
async def get_owners(service: OwnerService = Depends(inject_owner_service)):
    try:
        result: List[GetOwnerSchema] = await service.get_owners()
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@owner_router.get("/{owner_id}", response_model=GetOwnerSchema)
async def get_owner(owner_id: str, service: OwnerService = Depends(inject_owner_service)):
    try:
        result: GetOwnerSchema = await service.get_owner(owner_id)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@owner_router.post("", response_model=GetOwnerSchema, include_in_schema=False)
@owner_router.post("/", response_model=GetOwnerSchema)
async def create_owner(data: PostPutOwnerSchema, service: OwnerService = Depends(inject_owner_service)):
    try:
        return await service.add_owner(data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@owner_router.put("/{owner_id}", response_model=GetOwnerSchema)
async def update_owner(owner_id: str,
                       data: PostPutOwnerSchema,
                       service: OwnerService = Depends(inject_owner_service)):
    try:
        return await service.update_owner(owner_id, data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@owner_router.delete("/{owner_id}")
async def delete_owner(owner_id: str, service: OwnerService = Depends(inject_owner_service)) -> DeleteOwnerSchema:
    try:
        return await service.delete_owner(owner_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
