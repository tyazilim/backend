from typing import List

from fastapi import APIRouter, HTTPException, Depends

from api.services import MembershipService
from api.dependencies import inject_membership_service
from api.schemas.membership_schemas import *

__all__ = ["membership_router"]


membership_router = APIRouter(prefix="/memberships", tags=["memberships"], dependencies=[])


@membership_router.get("", response_model=List[GetMembershipSchema], include_in_schema=False)
@membership_router.get("/", response_model=List[GetMembershipSchema])
async def get_owners(service: MembershipService = Depends(inject_membership_service)):
    try:
        result: List[GetMembershipSchema] = await service.get_memberships()
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@membership_router.get("/{owner_id}", response_model=GetMembershipSchema)
async def get_owner(owner_id: str, service: MembershipService = Depends(inject_membership_service)):
    try:
        result: GetMembershipSchema = await service.get_membership(owner_id)
        return result
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@membership_router.post("", response_model=GetMembershipSchema, include_in_schema=False)
@membership_router.post("/", response_model=GetMembershipSchema)
async def create_owner(data: PostPutMembershipSchema, service: MembershipService = Depends(inject_membership_service)):
    try:
        return await service.add_membership(data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@membership_router.put("/{owner_id}", response_model=GetMembershipSchema)
async def update_owner(owner_id: str,
                       data: PostPutMembershipSchema,
                       service: MembershipService = Depends(inject_membership_service)):
    try:
        return await service.update_membership(owner_id, data)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@membership_router.delete("/{owner_id}")
async def delete_owner(owner_id: str, service: MembershipService = Depends(inject_membership_service)) -> DeleteMembershipSchema:
    try:
        return await service.delete_membership(owner_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
